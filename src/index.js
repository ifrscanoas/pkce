// Dependencia: crypto
// Referência https://auth0.com/docs/authorization/flows/call-your-api-using-the-authorization-code-flow-with-pkce
// Ferramenta online para testar https://tonyxu-io.github.io/pkce-generator/

const crypto = require("crypto")

// Gera o code_verifier, enviado na solicitação do authorization code.
function base64URLEncode(str) {
    return str.toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=/g, '');
}

// Gera o code_challenge, enviado na solicitação de token
function sha256(buffer) {
    return crypto.createHash('sha256').update(buffer).digest();
}

// Tamanho mínimo 43 e máximo 128 bytes https://datatracker.ietf.org/doc/html/rfc7636
const code_verifier = base64URLEncode(crypto.randomBytes(128));

const code_challenge = base64URLEncode(sha256(code_verifier));

console.log("code_verifier: " + code_verifier);
console.log("code_challenge: "+ code_challenge);
